﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CallTrackingLib.DataProvider;
using CallTrackingLib.Helpers;
using CsvHelper;
using CsvHelper.Configuration;
using DataImporter.CsvHelper;

namespace DataImporter
{
    internal static class SourceFiles
    {
        public static string Calls => "Calls_test.csv";
        public static string Events => "Events.csv";
        public static string AdsCalls => "Ads_calls.csv";
        public static string AdsPhones => "Ads_phones.csv";
        public static string Ads => "Ads.csv";

    }

    internal class SourceHelper
    {
        private const string inputPath = @"C:\h\_data\input";

        public static string InputPath => inputPath;

        public static string GetInputFileFullName(string fileName)
        {
            return Path.Combine(InputPath, fileName);
        }

        public static void ImportCSV<T>(string fileName,
            Action<hackathon_calltrackingDataContext, IEnumerable<T>> importer)
        {
            var counter = 0;

            using (var cntx = new hackathon_calltrackingDataContext())
            {
                using (var reader = new StreamReader(GetInputFileFullName(fileName)))
                using (var csv = new CsvReader(reader))
                {
                    csv.InitClassMappers<T>();

                    var records = csv.GetRecords<T>();

                    foreach (var batch in records.SplitToBatches(10000))
                    {
                        importer(cntx, batch);
                        cntx.SubmitChanges();
                        Console.WriteLine($"From file {fileName}\t{++counter}0 000 imported");
                    }
                }
            }
        }
    }
}
