﻿using CallTrackingLib.DataProvider;
using CsvHelper;
using CsvHelper.Configuration;

namespace DataImporter.CsvHelper
{
    internal static class ClassMappers
    {
        public static void InitClassMappers<T>(this CsvReader csv)
        {
            csv.Configuration.MissingFieldFound = null;
            csv.Configuration.Delimiter = "|";

            csv.Configuration.RegisterClassMap<EventsMap>();
            csv.Configuration.RegisterClassMap<AdsPhonesMap>();
            csv.Configuration.RegisterClassMap<AdsMap>();
            csv.Configuration.RegisterClassMap<AdsCallMap>();
        }
    }

    public sealed class EventsMap : ClassMap<@event>
    {
        public EventsMap()
        {
            AutoMap();
            Map(m => m.id).Ignore();
        }
    }

    public sealed class AdsPhonesMap : ClassMap<ads_phone>
    {
        public AdsPhonesMap()
        {
            AutoMap();
            Map(m => m.id).Ignore();
        }
    }

    public sealed class AdsMap : ClassMap<ad>
    {
        public AdsMap()
        {
            AutoMap();
            Map(m => m.id).Ignore();
        }
    }

    public sealed class AdsCallMap : ClassMap<ads_call>
    {
        public AdsCallMap()
        {
            AutoMap();
            Map(m => m.id).Ignore();
        }
    }
}
