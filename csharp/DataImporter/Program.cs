﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using CallTrackingLib.DataProvider;
using CsvHelper;
using CsvHelper.Configuration;

namespace DataImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.WriteLine("Start.");

            
            // Импорт звонков
            SourceHelper.ImportCSV<call>(SourceFiles.Calls, (cntx, calls) =>
            {
                cntx.calls.InsertAllOnSubmit(calls);
            });

            /*
            // Импорт событий
            SourceHelper.ImportCSV<@event>(SourceFiles.Events, (cntx, events) =>
            {
                cntx.events.InsertAllOnSubmit(events);
            });

            // Импорт связь объявление - телефоны
            SourceHelper.ImportCSV<ads_phone>(SourceFiles.AdsPhones, (cntx, adsPhones) =>
            {
                cntx.ads_phones.InsertAllOnSubmit(adsPhones);
            });
            */

            /*
            // Импорт связь объявление - телефоны
            SourceHelper.ImportCSV<ads_call>(SourceFiles.AdsCalls, (cntx, adsPhones) =>
            {
                cntx.ads_calls.InsertAllOnSubmit(adsPhones);
            });
            */
            /*
            // Импорт объявлений
            SourceHelper.ImportCSV<ad>(SourceFiles.Ads, (cntx, ads) =>
            {
                cntx.ads.InsertAllOnSubmit(ads);
            });*/


            Console.WriteLine("Done.");
        }
    }
}
