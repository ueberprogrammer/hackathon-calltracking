﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CallTrackingLib.DataProvider;

namespace CallTrackingLib.Helpers
{
    public static class AutoMapperHelper
    {
        private static MapperConfiguration config;
        public static Mapper Mapper { get; set; }

        static AutoMapperHelper()
        {
            config = new MapperConfiguration(cfg => cfg.CreateMap<P_GetCalls_WithAnswersResult, P_GetCallsResult>());

            Mapper = new Mapper(config);
        }
    }
}
