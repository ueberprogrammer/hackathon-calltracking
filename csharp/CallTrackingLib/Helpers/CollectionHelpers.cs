﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Helpers
{
    public static class CollectionHelpers
    {
        public static IEnumerable<T[]> SplitToBatches<T>(
            this IEnumerable<T> items,
            int batchSize)
        {
            if (items != null)
            {
                using (var iterator = items.GetEnumerator())
                {
                    while (iterator.MoveNext())
                    {
                        var array = new T[batchSize];
                        array[0] = iterator.Current;
                        int newSize;
                        for (newSize = 1; newSize < batchSize && iterator.MoveNext(); ++newSize)
                        {
                            array[newSize] = iterator.Current;
                        }

                        Array.Resize<T>(ref array, newSize);
                        yield return array;
                    }
                }
            }
        }
    }
}
