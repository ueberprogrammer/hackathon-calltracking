﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("CallTrackingLib")]
[assembly: AssemblyDescription("Cian CallTracking Hackathon - Цветов & Неронов")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Cian")]
[assembly: AssemblyProduct("CallTrackingLib")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("3db341db-8aca-491f-be03-af67f32e7cde")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("CallTrackingConsole")]
[assembly: InternalsVisibleTo("CallTrackingApp")]
[assembly: InternalsVisibleTo("DataImporter")]

