﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Models
{
    public class Statistic
    {
        public int Right { get; set; }

        public int Wrong { get; set; }

        public float GetPercent()
        {
            if (Right == 0 && Wrong == 0)
                return 0;

            if (Right != 0)
                return Right * 100 / (Right + Wrong);
            else
                return 0;
        }
    }
}
