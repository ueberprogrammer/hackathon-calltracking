﻿using System.Collections.Generic;
using System.Linq;
using Ads = CallTrackingLib.DataProvider.ads_single;

namespace CallTrackingLib.Models
{
    public class AdScore
    {
        public AdScore(Ads ad, double score)
        {
            this.Ad = ad;
            this.Score = score;
        }

        public double Score { get; set; }

        public Ads Ad { get; set; }
    }

    public static class AdScoreHelper
    {
        public static void UppedScore(this List<AdScore> scores, Ads ad, double score)
        {
            var existScore = scores.FirstOrDefault(x => x.Ad.realtyid == ad.realtyid);

            if (existScore != null)
                existScore.Score += score;
            else
                scores.Add(new AdScore(ad, score));
        }
        
        public static long? GetRealtyId(this List<AdScore> scores)
        {
            return scores.OrderByDescending(x => x.Score).FirstOrDefault()?.Ad?.realtyid;
        }

        public static double GetScore(this List<AdScore> scores)
        {
            return scores.OrderByDescending(x => x.Score)?.FirstOrDefault()?.Score ?? 0;
        }
    }
}
