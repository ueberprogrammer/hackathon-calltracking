﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Models
{
    public class PythonCurl
    {
        const string ApiUrl = "http://172.20.14.20/search/";

        public static async Task<string> Search(CurlSearchModelV2 model)
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), ApiUrl))
                {
                    string destJson = JsonConvert.SerializeObject(model);

                    request.Content = new StringContent(destJson, Encoding.UTF8, "application/json");

                    var result = await httpClient.SendAsync(request);

                    return result.Content.ReadAsStringAsync().Result;
                }
            }
        }
    }
}
