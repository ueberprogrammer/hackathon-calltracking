﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Models
{

    public class GeoModel
    {
        public int countryId { get; set; }
        public object[] undergrounds { get; set; }
        public object[] calculatedUndergrounds { get; set; }
        public Coordinates coordinates { get; set; }
        public object[] highways { get; set; }
        public object[] railways { get; set; }
        public string userInput { get; set; }
        public Address[] address { get; set; }
        public object[] district { get; set; }
        public Locationpath locationPath { get; set; }
    }

    public class Coordinates
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }

    public class Locationpath
    {
        public int countryId { get; set; }
        public int[] childToParent { get; set; }
    }

    public class Address
    {
        public string name { get; set; }
        public int id { get; set; }
        public int locationTypeId { get; set; }
        public string fullName { get; set; }
        public string type { get; set; }
        public string shortName { get; set; }
        public bool isFormingAddress { get; set; }
    }

}
