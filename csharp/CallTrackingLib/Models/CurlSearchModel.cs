﻿namespace CallTrackingLib.Models
{
    public class CurlSearchModel
    {
        public CurlSearchModel(CurlSearchModelV2 m)
        {
            this.id_user = m.id_user;
            this.region = m.region;
            this.search_deal = m.search_deal;
            this.search_worlds = m.search_worlds;
        }

        public long id_user { get; set; }
        public string region { get; set; }
        public string[] search_deal { get; set; }
        public string[] search_worlds { get; set; }
    }

    public class CurlSearchModelV2
    {
        public long id_user { get; set; }
        public string region { get; set; }
        public string dest_phone;
        public long? realtyId;
        public string[] search_deal { get; set; }
        public string[] search_worlds { get; set; }
    }
}

