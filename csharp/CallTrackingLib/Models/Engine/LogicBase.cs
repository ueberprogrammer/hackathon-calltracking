﻿using CallTrackingLib.DataProvider;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Models.Engine
{
    public class LogicBase
    {
        public List<P_GetCallsResult> Calls { get; set; }
        public Dictionary<long?, long?> Answers { get; set; }
        protected readonly Action<string> logger;

        protected readonly bool checkWithAnswers = false;

        public LogicBase(Action<string> logger, bool checkWithAnswers)
        {
            this.logger = logger;
            this.checkWithAnswers = checkWithAnswers;

            Answers = DbMethods.GetAnswers();
            Calls = DbMethods.GetCalls(checkWithAnswers);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void SaveResult(string fileName, P_GetCallsResult call, List<AdScore> adScores)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Append))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine($"{call.calltrackingid}|{adScores.GetRealtyId()}");
            }
        }
        
        protected int PrintOutputInfo(List<P_GetCallsResult> calls, Statistic statistic, P_GetCallsResult call, List<AdScore> adScores, ref int counter)
        {
            if (checkWithAnswers)
            {
                var validStr = string.Empty;
                var isValidAnswer = this.Answers[call.calltrackingid] == adScores.GetRealtyId();
                var containsRight = adScores.FirstOrDefault(x => x.Ad.realtyid == this.Answers[call.calltrackingid]);

                if (containsRight != null && isValidAnswer == false)
                {
                    validStr = $" answer: {containsRight.Ad.realtyid}\t score: {containsRight.Score}";
                }

                if (isValidAnswer)
                    statistic.Right++;
                else
                    statistic.Wrong++;


                this.logger($"[{counter++}/{calls.Count}] \t {call.calltrackingid} \t purpose:{adScores.GetRealtyId()} [ads: {adScores.Count}] \t score: {adScores.GetScore()} \t {isValidAnswer}\t percent: <{statistic.GetPercent()}%> \t \t {validStr}");
            }
            else
                this.logger($"\n[{counter++}/{calls.Count}] \t {call.calltrackingid} \t purpose:{adScores.GetRealtyId()} \n [ads: {adScores.Count}] \t score: {adScores.GetScore()} ");

            /*
            foreach (AdScore score in adScores)
            {
                this.logger($" \t\t\t\t realty_id: {score.Ad.realtyid}, score: {score.Score}");
            } */

            return counter;
        }
    }
}
