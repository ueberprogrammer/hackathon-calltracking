﻿using CallTrackingLib.DataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using CallTrackingLib.Helpers;

namespace CallTrackingLib.Models.Engine
{
    public static class DbMethods
    {
        public static List<P_GetCallsResult> GetCalls(bool withAnswers = false)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return withAnswers 
                    ? cntx.P_GetCalls_WithAnswers()
                        .Select(x=> AutoMapperHelper.Mapper.Map<P_GetCallsResult>(x))
                        .ToList()
                    : cntx.P_GetCalls()
                        .ToList();
            }
        }

        public static List<P_GetAdsByPhoneAndDateResult> GetAdsByPhoneAndDate(string phoneHash, DateTime? date)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return cntx.P_GetAdsByPhoneAndDate(phoneHash, date).ToList();
            }
        }

        public static List<long> GetAdsIdsByPhoneAndDate(string phoneHash, DateTime? date)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return cntx.P_GetAdIdsByPhoneAndDate(phoneHash, date).Select(x=>x.realtyid).Cast<long>().ToList();
            }
        }

        public static List<P_GetEventsByDateResult> GetEventsByDate(DateTime? date)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return cntx.P_GetEventsByDate(date).ToList();
            }
        }

        public static ad GetAdByRealtyId(long realtyId)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return cntx.ads.FirstOrDefault(x=>x.realtyid == realtyId);
            }
        }

        public static List<long> GetAdsIds(this List<P_GetAdsByPhoneAndDateResult> getResultAds)
        {
            return getResultAds.Where(x => x.realtyid != null)
                  .Select(x => x.realtyid)
                  .Cast<long>()
                  .Distinct()
                  .ToList();
        }

        public static List<ads_single> GetAds(this List<long> adsIds)
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                return adsIds
                    .Select(realtyid => cntx.ads_singles.FirstOrDefault(x => x.realtyid == realtyid))
                    .ToList();
            }
        }

        public static Dictionary<long?, long?> GetAnswers()
        {
            using (var cntx = new hackathon_calltrackingDataContext())
            {
                var dict = cntx.rasmetkas.ToDictionary(x => x.calltrackingid, y => y.realtyid);

                return dict;
            }
        }
    }
}
