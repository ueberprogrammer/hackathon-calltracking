﻿using CallTrackingLib.DataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CallTrackingLib.Models.Engine
{
    public static class SearchTextMethods
    {
        public static string[] GetUniqueWords(string txt)
        {
            txt = new Regex("[^а-яА-Я0-9]").Replace(txt, " ");
            
            var words = txt.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var wordQuery = words.Distinct();

            return words.ToArray();
        }

        public static string[] GetDealWords(string str)
        {
            var list = new List<string>();

            if (str.ToLower().Contains("sale"))
                list.Add("Прода");
            else
            {
                list.Add("Аренд");
                list.Add("Сдает");
                list.Add("Сдаёт");
                list.Add("Снят");
            }

            if (str.ToLower().Contains("flat"))
                list.Add("Квартир");


            if (str.ToLower().Contains("flat"))
                list.Add("Комнат");

            if (str.ToLower().Contains("garage"))
                list.Add("Гараж");

            if (str.ToLower().Contains("land"))
            {
                list.Add("Земл");
                list.Add("километров");
            }

            if(str.ToLower().Contains("officeRent"))
            {
                list.Add("Офис");
            }

            if (str.ToLower().Contains("house"))
            {
                list.Add("Загород");
                list.Add("Дом");
                list.Add("Дач");
                list.Add("километров");
            }

            return list.ToArray();
        }

        public static string GetRoomsCountWord(int roomsCount)
        {
            switch (roomsCount)
            {
                case 1:
                    return "однокомнат";
                case 2:
                    return "двухкомнат";
                case 3:
                    return "стрехкомнат";
                case 4:
                    return "четырехкомнат";
                case 5:
                    return "пятникомнат";
            }

            return "not valid";
        }

        public static string Crop(this string txt, int count = 4)
        {
            if (txt.Length < 5)
                return "not valid";

            if (txt.Length < 9)
                return txt;

            return txt.Substring(0, txt.Length - count);
        }

        public static bool IsContainsWord(this P_GetCallsResult call, string word)
        {
            return call.incomingtext.ToLowerInvariant().Contains(word.ToLowerInvariant())
                || call.outgoingtext.ToLowerInvariant().Contains(word.ToLowerInvariant());
        }

        public static bool IsOutcomingContainsWord(this P_GetCallsResult call, string word)
        {
            return call.outgoingtext.ToLowerInvariant().Contains(word.ToLowerInvariant());
        }

        public static bool IsIncomingContainsWord(this P_GetCallsResult call, string word)
        {
            return call.incomingtext.ToLowerInvariant().Contains(word.ToLowerInvariant());
        }

        public static bool IsContainsAnyWord(this P_GetCallsResult call, string[] words)
        {
            return words.Any(word => call.incomingtext.ToLowerInvariant().Contains(word.ToLowerInvariant()))
                || words.Any(word => call.outgoingtext.ToLowerInvariant().Contains(word.ToLowerInvariant()));
        }


        public static int GetCountContainsWord(this P_GetCallsResult call, string[] words)
        {
            return words.Count(word => call.incomingtext.ToLowerInvariant().Contains(word.ToLowerInvariant()))
                   + words.Count(word => call.outgoingtext.ToLowerInvariant().Contains(word.ToLowerInvariant()));
        }
    }
}
