﻿using CallTrackingLib.DataProvider;
using CallTrackingLib.Models.Engine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Ads = CallTrackingLib.DataProvider.ads_single;

namespace CallTrackingLib.Models
{
    /// <summary>
    /// Алгоритм распознавания объявления
    /// Шаг 1. Итерирую разговоры
    /// Шаг 2. Получаю объявления по исходному телефону автора и дате публикации
    /// Шаг 3. Фильтрую полученные объявления по событиям (расхлоп не ранее 2 дня и не позже)
    /// Шаг 4. Скоринг объявлений по вхождению слов
    /// Шаг 5. Скоринг по расхопам
    /// Шаг 6. ElasticSearch - Хожу в python ручку curl (сопостовление объявки к звонку по словам)
    /// </summary>
    public class RecognizeLogic : LogicBase
    {
        public RecognizeLogic(Action<string> logger, bool checkWithAnswers) : base(logger, checkWithAnswers) { }

        // Распознать объявки
        public RecognazeResult Recognize()
        {
            var result = FilterAds_Step1(Calls);

            return result;
        }

        // Шаг 1. Итерирую разговоры
        private RecognazeResult FilterAds_Step1(List<P_GetCallsResult> calls)
        {
            var counter = 1;

            var statistic = new Statistic();

            var result = new RecognazeResult();

            foreach (var call in calls)
            {
                var events = DbMethods.GetEventsByDate(call.date);

                // Шаг 2. Получаю объявления по исходному телефону автора и дате публикации
                var ads = FilterAds_Step2(call);

                // Шаг 3. Фильтрую полученные объявления по событиям (расхлоп не ранее 2 дня и не позже)
                ads = FilterAds_Step3(events, call, ads);

                // Шаг 4. Скоринг объявлений по вхождению слов
                var adScores = FilterAds_Step4(call, ads);

                // Шаг 5. Скоринг по расхопам
                adScores = FilterAds_Step5(adScores, events, call, ads);

                // Шаг 6.ElasticSearch - Хожу в python ручку curl(сопостовление объявки к звонку по словам)
                adScores = FilterAds_Step6(adScores, call, ads);

                result.Add(new RecognizeItem(call, adScores));

                // Логирую отладочную ифнормацию
                PrintOutputInfo(calls, statistic, call, adScores, ref counter);

                // Сохраняю результат в файл
                SaveResult(@"C:\neronov.csv", call, adScores);
            }


            return result;
        }

        // Шаг 2. Получаю объявления по исходному телефону автора и дате публикации
        public List<Ads> FilterAds_Step2(P_GetCallsResult call)
        {
            var ids = DbMethods.GetAdsIdsByPhoneAndDate(call.destinationphone_hash, call.date);

            var ads = ids.GetAds();

            var realtyIds = ads.Select(x => x.realtyid).Distinct();

            return realtyIds.Select(x => ads.First(p => p.realtyid == x)).ToList();
        }

        // Шаг 3. Фильтрую полученные объявления по событиям (есть расхлоп не ранее 2 дня и не позже)
        public List<Ads> FilterAds_Step3(List<P_GetEventsByDateResult> events, P_GetCallsResult call, List<Ads> ads)
        {
            ads = ads.Where(x => events.Any(e => e.realtyid == x.realtyid)).ToList();

            return ads;
        }

        // Шаг 4. Скоринг объявлений по вхождению слов
        public List<AdScore> FilterAds_Step4(P_GetCallsResult call, List<Ads> ads)
        {
            var scoreAds = new List<AdScore>();

            foreach (var ad in ads)
            {
                if (ad?.realtyid == null)
                {
                    continue;
                }

                var geo = JsonConvert.DeserializeObject<GeoModel>(ad.geo);

                var street = geo.address.FirstOrDefault(x => x.type == "street");

                var house = geo.address.FirstOrDefault(x => x.type == "house");

                if (street != null)
                {
                    var streetName = street.name.Crop();

                    if (call.IsOutcomingContainsWord(streetName))
                    {
                        scoreAds.UppedScore(ad, 270 + 56 + 57);
                    }

                    if (call.IsIncomingContainsWord(streetName))
                    {
                        scoreAds.UppedScore(ad, 200);
                    }
                }

                if (house != null)
                {
                    if (call.IsContainsWord(house.name))
                    {
                        scoreAds.UppedScore(ad, 100);
                    }
                }

                if (call.IsContainsWord(ad.pricerub.ToString().Substring(0, 3)))
                {
                    scoreAds.UppedScore(ad, 25);
                }

                var roomsCountWords = SearchTextMethods.GetRoomsCountWord((int?) ad.roomscount ?? 0);

                if (call.IsContainsWord(roomsCountWords))
                {
                    scoreAds.UppedScore(ad, 200);
                }

                var categoryWords = SearchTextMethods.GetDealWords(ad.category);

                if (call.IsContainsAnyWord(categoryWords))
                {
                    scoreAds.UppedScore(ad, 200);
                }

                scoreAds.UppedScore(ad, call.GetCountContainsWord(categoryWords) * 101);

                var adressWords = SearchTextMethods.GetUniqueWords(ad.geo_userinput);

                if (call.IsContainsAnyWord(adressWords))
                {
                    scoreAds.UppedScore(ad, 52);
                }

                scoreAds.UppedScore(ad, call.GetCountContainsWord(adressWords) * 5);

                var descriptionWords = SearchTextMethods.GetUniqueWords(ad.description);

                if (call.IsContainsAnyWord(descriptionWords))
                {
                    scoreAds.UppedScore(ad, 43);
                }

                scoreAds.UppedScore(ad, call.GetCountContainsWord(adressWords) * 10);
            }

            return scoreAds;
        }

        // Шаг 5. Скоринг по расхопам
        private List<AdScore> FilterAds_Step5(List<AdScore> adScores, List<P_GetEventsByDateResult> events, P_GetCallsResult call, List<Ads> ads)
        {
            // Добавить очки популярным объявкам
            foreach (var ad in ads)
                adScores.UppedScore(ad, events.Count(x => x.realtyid == (long)ad.realtyid) * 5);

            // Добавить очки объявкам в зависимости от времени последнего расхлопа
            var scoresByNearly = new []
            {
                // from min, to min, score
                new [] {0, 120, 40},
                new [] {120, 150, 30},
                new [] {150, 200, 20},
                new [] {200, 300, 10},
                new [] {300, 500, 5},
            };

            foreach (var defines in scoresByNearly)
            {
                ScoreByNearlyViewPhone(adScores, events, ads,
                    fromMinutes: defines[0],
                    toMinutes: defines[1],
                    score: defines[2]);
            }

            return adScores;
        }

        private void ScoreByNearlyViewPhone(List<AdScore> adScores, List<P_GetEventsByDateResult> events, List<Ads> ads, int fromMinutes, int toMinutes, int score)
        {
            var quicklyAds =
                ads.Where(ad => events
                    .Any(e => (e.event_timestamp - ad.ptn_dadd).Value.Minutes <= toMinutes
                              && (fromMinutes == 0 || (e.event_timestamp - ad.ptn_dadd).Value.Minutes > fromMinutes)));

            foreach (var ad in quicklyAds)
                adScores.UppedScore(ad, score);
        }

        /// Шаг 6. ElasticSearch - Хожу в python ручку curl (сопостовление объявки к звонку по словам)
        public List<AdScore> FilterAds_Step6(List<AdScore> scoreAds, P_GetCallsResult call, List<Ads> ads)
        {
            foreach (var ad in ads)
            {
                if (ad?.realtyid == null)
                    continue;

                var geo = JsonConvert.DeserializeObject<GeoModel>(ad.geo);

                var regionName = "";

                if (geo?.address?.Length != 0)
                {
                    regionName = geo.address[0].name;
                }

                // Запрос в elastic через python ручку
                var result = PythonCurl.Search(new CurlSearchModelV2()
                {
                    dest_phone = call.destinationphone_hash,
                    id_user = (long)call.realtyuserid,
                    realtyId = ad.realtyid,
                    region = regionName,
                    search_deal = SearchTextMethods.GetDealWords(ad.category),
                    search_worlds = SearchTextMethods.GetUniqueWords(ad.geo_userinput)
                }).Result;

                if (result.Contains(call.calltrackingid.ToString()))
                {
                    scoreAds.UppedScore(ad, 150);
                    logger($" * * * информация от ручки curl - подолша объявка: {ad.realtyid}! * * *");
                }
            }

            return scoreAds;
        }
    }
}
