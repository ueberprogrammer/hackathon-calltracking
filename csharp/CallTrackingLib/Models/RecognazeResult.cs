﻿using CallTrackingLib.DataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallTrackingLib.Models
{
    public class RecognazeResult
    {
        public List<RecognizeItem> Items { get; set; }

        public RecognazeResult()
        {
            this.Items = new List<RecognizeItem>();
        }

        public void Add(RecognizeItem item)
        {
            this.Items.Add(item);
        }
    }


    public class RecognizeItem
    {
        public long CallId { get; set; }
        public List<ScoreResult> Ads { get; set; }

        public RecognizeItem(P_GetCallsResult call, List<AdScore> adScores)
        {
            this.CallId = call.calltrackingid;
            this.Ads = adScores.Select(x => new ScoreResult(x)).OrderByDescending(x=>x.Score).ToList();
        }
    }
    

    public class ScoreResult
    {
        public ScoreResult(AdScore adScore)
        {
            this.RealtyId = (long)adScore.Ad.realtyid;
            this.Score = adScore.Score;
        }

        public long RealtyId { get; set; }
        public double Score { get; set; }
    }
}
