﻿using CallTrackingLib.Models;
using System;
using System.Text;

namespace CallTrackingConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            RecognizeLogic logic = new RecognizeLogic(  (str) =>  { Console.WriteLine(str); },
                                                        checkWithAnswers: true);

            Console.WriteLine("Start recognize");

            RecognazeResult result = logic.Recognize();

            Console.WriteLine("Done");

        }
    }
}
