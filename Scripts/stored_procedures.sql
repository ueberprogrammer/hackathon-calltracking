CREATE PROCEDURE [dbo].[P_GetAdsByPhoneAndDate] @phonehash NVARCHAR(1024)
	,@date DATETIME2
AS
BEGIN
	SELECT a.[realtyid]
		,a.[userid]
		,[publisheduserid]
		,a.[ptn_dadd]
		,[creationdate]
		,[isactive]
		,[wasactive]
		,[geo_userinput]
		,[geo_address]
		,[geo]
		,[category]
		,[floornumber]
		,[floorscount]
		,[roomscount]
		,[bargainterms]
		,[description]
		,[squarefull]
		,[landarea]
		,[building]
		,[repairtype]
		,[pricerub]
		,[geo_district]
		,[street]
		,[house]
	FROM [hackathon_calltracking].[dbo].[ads_phones] p
	LEFT JOIN dbo.ads a WITH (NOLOCK) ON a.realtyid = p.realtyid
	WHERE p.[destinationphone_hash] = @phonehash
		AND p.ptn_dadd <= @date
END

GO

CREATE PROCEDURE [dbo].[P_GetEventsByDate] @date DATETIME2
AS
BEGIN
	SELECT [id]
		,[realtyid]
		,[ab_group]
		,[hit_id]
		,[event_type]
		,[event_timestamp]
	FROM [hackathon_calltracking].[dbo].[events]
	WHERE event_timestamp <= @date
		AND event_type = N'phone'
END

GO

CREATE PROCEDURE [dbo].[P_GetCalls_1]
AS
BEGIN
	SELECT TOP 100 [calltrackingid]
		,[realtyuserid]
		,[calltrackingphone_hash]
		,[sourcephone_hash]
		,[destinationphone_hash]
		,[outgoingfilepath_hash]
		,[incomingfilepath_hash]
		,[mixedfilepath_hash]
		,[incomingtext]
		,[outgoingtext]
		,[jsonincoming]
		,[jsonoutgoing]
		,[region]
		,[talkduration]
		,[date]
		,[region__calltracing_number]
		,[rn]
	FROM [hackathon_calltracking].[dbo].[calls] calls
	WHERE cast([date] AS DATE) >= cast('2019-07-27' AS DATE)
END

GO

CREATE PROCEDURE [dbo].[P_GetAdsByPhoneAndDate1] @phonehash NVARCHAR(1024)
	,@date DATETIME2
AS
BEGIN
	SELECT [id]
		,[realtyid]
		,[userid]
		,[ptn_dadd]
		,[destinationphone_hash]
	FROM [hackathon_calltracking].[dbo].[ads_phones]
	WHERE [destinationphone_hash] = @phonehash
		AND ptn_dadd <= @date
END

GO

SELECT count(*)
FROM ads_single WITH (NOLOCK)

CREATE PROCEDURE [dbo].[P_GetAdIdsByPhoneAndDate] @phonehash NVARCHAR(1024)
	,@date DATETIME2
AS
BEGIN
	SELECT DISTINCT p.[realtyid]
	FROM [hackathon_calltracking].[dbo].[ads_phones] p
	WHERE p.[destinationphone_hash] = @phonehash
		AND p.ptn_dadd <= @date
END

GO --select top 100 * from dbo.ads_single with(nolock)

CREATE PROCEDURE [dbo].[P_GetCalls]
AS
BEGIN
	SELECT [calltrackingid]
		,[realtyuserid]
		,[calltrackingphone_hash]
		,[sourcephone_hash]
		,[destinationphone_hash]
		,[outgoingfilepath_hash]
		,[incomingfilepath_hash]
		,[mixedfilepath_hash]
		,[incomingtext]
		,[outgoingtext]
		,[jsonincoming]
		,[jsonoutgoing]
		,[region]
		,[talkduration]
		,[date]
		,[region__calltracing_number]
		,[rn]
	FROM [hackathon_calltracking].[dbo].[calls] calls
END

GO

CREATE PROCEDURE [dbo].[P_GetAdsByRealtyUserId] @realtyUserId BIGINT
AS
BEGIN
	SELECT [calltrackingid]
		,[realtyuserid]
		,[calltrackingphone_hash]
		,[sourcephone_hash]
		,ap.[destinationphone_hash]
		,[outgoingfilepath_hash]
		,[incomingfilepath_hash]
		,[mixedfilepath_hash]
		,[incomingtext]
		,[outgoingtext]
		,[jsonincoming]
		,[jsonoutgoing]
		,[region]
		,[talkduration]
		,[date]
		,[region__calltracing_number]
		,[rn]
	FROM [hackathon_calltracking].[dbo].[calls] calls
	LEFT JOIN [hackathon_calltracking].[dbo].ads_phones ap ON ap.[destinationphone_hash] = calls.[destinationphone_hash]
	WHERE realtyuserid = @realtyUserId
		AND ap.id IS NOT NULL
END

GO

CREATE PROCEDURE [dbo].[P_GetValidCallsAds]
AS
BEGIN
	SELECT TOP 10000 r.calltrackingid
		,r.[realtyuserid]
		,a.category
		,a.geo
		,a.street
		,p.[destinationphone_hash]
		,r.realtyid
		,a.[description]
		,a.[geo_userinput]
		,a.street
	FROM [hackathon_calltracking].[dbo].[rasmetka] r
	LEFT JOIN [hackathon_calltracking].[dbo].[ads] a ON a.[realtyid] = r.realtyid
	LEFT JOIN [hackathon_calltracking].[dbo].calls c ON c.calltrackingid = r.calltrackingid
	LEFT JOIN [hackathon_calltracking].[dbo].ads_phones p ON p.[destinationphone_hash] = c.[destinationphone_hash]
	WHERE a.realtyid IS NOT NULL
		AND p.[destinationphone_hash] IS NOT NULL
END

GO

CREATE PROCEDURE P_GetCallsByWord @word NVARCHAR(1024)
AS
BEGIN
	SELECT *
	FROM dbo.calls
	WHERE CONTAINS (
			[incomingtext]
			,@word
			)
END