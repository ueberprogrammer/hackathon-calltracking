﻿USE [hackathon_calltracking]

GO

CREATE TABLE [dbo].[ads](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[realtyid] [bigint] NULL,
	[userid] [bigint] NULL,
	[publisheduserid] [bigint] NULL,
	[ptn_dadd] [datetime2](7) NULL,
	[creationdate] [datetime2](7) NULL,
	[isactive] [int] NULL,
	[wasactive] [int] NULL,
	[geo_userinput] [ntext] NULL,
	[geo_address] [ntext] NULL,
	[geo] [ntext] NULL,
	[category] [nvarchar](50) NULL,
	[floornumber] [float] NULL,
	[floorscount] [float] NULL,
	[roomscount] [float] NULL,
	[bargainterms] [ntext] NULL,
	[description] [ntext] NULL,
	[squarefull] [float] NULL,
	[landarea] [float] NULL,
	[building] [ntext] NULL,
	[repairtype] [nvarchar](50) NULL,
	[pricerub] [money] NULL,
	[geo_district] [ntext] NULL,
	[street] [nvarchar](max) NULL,
	[house] [nvarchar](50) NULL,
 CONSTRAINT [PK_ads1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[ads_calls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[calltrackingid] [bigint] NULL,
	[realtyuserid] [bigint] NULL,
	[calldate] [datetime2](7) NULL,
	[realtyid] [bigint] NULL,
	[creationdate] [datetime2](7) NULL,
	[isactive] [int] NULL,
	[wasactive] [int] NULL,
	[ptn_dadd] [date] NULL,
	[geo_userinput] [nvarchar](max) NULL,
 CONSTRAINT [PK_ads_calls] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[ads_phones](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[realtyid] [bigint] NULL,
	[userid] [bigint] NULL,
	[ptn_dadd] [date] NULL,
	[destinationphone_hash] [nvarchar](512) NULL,
 CONSTRAINT [PK_ads_phones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ads_single](
	[id] [bigint] NOT NULL,
	[realtyid] [bigint] NULL,
	[userid] [bigint] NULL,
	[publisheduserid] [bigint] NULL,
	[ptn_dadd] [datetime2](7) NULL,
	[creationdate] [datetime2](7) NULL,
	[isactive] [int] NULL,
	[wasactive] [int] NULL,
	[geo_userinput] [ntext] NULL,
	[geo_address] [ntext] NULL,
	[geo] [ntext] NULL,
	[category] [nvarchar](50) NULL,
	[floornumber] [float] NULL,
	[floorscount] [float] NULL,
	[roomscount] [float] NULL,
	[bargainterms] [ntext] NULL,
	[description] [ntext] NULL,
	[squarefull] [float] NULL,
	[landarea] [float] NULL,
	[building] [ntext] NULL,
	[repairtype] [nvarchar](50) NULL,
	[pricerub] [money] NULL,
	[geo_district] [ntext] NULL,
	[street] [nvarchar](max) NULL,
	[house] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[calls](
	[calltrackingid] [bigint] NOT NULL,
	[realtyuserid] [bigint] NULL,
	[calltrackingphone_hash] [nvarchar](4000) NULL,
	[sourcephone_hash] [nvarchar](4000) NULL,
	[destinationphone_hash] [nvarchar](4000) NULL,
	[outgoingfilepath_hash] [nvarchar](4000) NULL,
	[incomingfilepath_hash] [nvarchar](4000) NULL,
	[mixedfilepath_hash] [nvarchar](4000) NULL,
	[incomingtext] [ntext] NULL,
	[outgoingtext] [ntext] NULL,
	[jsonincoming] [ntext] NULL,
	[jsonoutgoing] [ntext] NULL,
	[region] [nvarchar](4000) NULL,
	[talkduration] [bigint] NULL,
	[date] [datetime2](7) NULL,
	[region__calltracing_number] [nvarchar](4000) NULL,
	[rn] [bigint] NULL,
 CONSTRAINT [PK_calls_test] PRIMARY KEY CLUSTERED 
(
	[calltrackingid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[calls_old](
	[calltrackingid] [bigint] NOT NULL,
	[realtyuserid] [bigint] NULL,
	[calltrackingphone_hash] [nvarchar](4000) NULL,
	[sourcephone_hash] [nvarchar](4000) NULL,
	[destinationphone_hash] [nvarchar](4000) NULL,
	[outgoingfilepath_hash] [nvarchar](4000) NULL,
	[incomingfilepath_hash] [nvarchar](4000) NULL,
	[mixedfilepath_hash] [nvarchar](4000) NULL,
	[incomingtext] [ntext] NULL,
	[outgoingtext] [ntext] NULL,
	[jsonincoming] [ntext] NULL,
	[jsonoutgoing] [ntext] NULL,
	[region] [nvarchar](4000) NULL,
	[talkduration] [bigint] NULL,
	[date] [datetime2](7) NULL,
	[region__calltracing_number] [nvarchar](4000) NULL,
	[rn] [bigint] NULL,
 CONSTRAINT [PK_calls] PRIMARY KEY CLUSTERED 
(
	[calltrackingid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[events](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[realtyid] [bigint] NULL,
	[ab_group] [float] NULL,
	[hit_id] [nvarchar](512) NULL,
	[event_type] [nvarchar](50) NULL,
	[event_timestamp] [datetime2](7) NULL,
 CONSTRAINT [PK_events] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[rasmetka](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[calltrackingid] [bigint] NULL,
	[address] [nvarchar](max) NULL,
	[flag_all] [nvarchar](max) NULL,
	[realtyuserid] [bigint] NULL,
	[realtyid] [bigint] NULL,
	[comment] [nvarchar](max) NULL,
	[who] [nvarchar](max) NULL,
	[bkt] [nvarchar](max) NULL,
 CONSTRAINT [PK_rasmetka] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[results](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[call_id] [bigint] NULL,
	[realty_id] [bigint] NULL,
	[scoreData] [ntext] NULL,
	[tw] [datetime2](7) NULL,
 CONSTRAINT [PK_results] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
